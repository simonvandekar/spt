rm(list=ls())
# directory to output
bootdir = file.path(getwd(), 'spt_out')
nboot<-5000


### SPEARMAN ###
boot<-readRDS(file.path(bootdir, 'perm1.rds'))
master.cors<-array(dim=c(nboot, dim(boot)))
	for(i in 1:nboot){
		master.cors[i,,]<-readRDS(file.path(bootdir, paste('perm', i, '.rds', sep='')))
	}
	master.cors<-master.cors[, grep('rotated', rownames(boot), invert=TRUE), grep('rotated', colnames(boot))]
	dimnames(master.cors)<-list(NULL, grep('rotated', rownames(boot), invert=TRUE, value=TRUE), grep('rotated', colnames(boot), value=TRUE) )

###  GET P-VALUES BY COMPARING WITH ACTUAL VALUES ###
# computes the actual correlations
vox.data.cors = readRDS( file.path(getwd(), 'data_frame_format.rds') )
vox.data.cors<-na.omit(vox.data.cors)
measures<-c(grep('_cor.age|_mean', names(vox.data.cors), value=TRUE), grep('^thickness_pc', names(vox.data.cors), value=TRUE))
cors<-cor(vox.data.cors[,measures], use='pairwise.complete.obs', method='spearman')

## SPEARMAN pvalues ##
pvalues.cors<-apply(sweep(abs(master.cors), c(2,3), abs(cors), ">="), c(2,3), mean, na.rm=TRUE)
colnames(pvalues.cors)<-rownames(pvalues.cors)<-colnames(cors)
# each rotation actually gives two estimates of pvalues b/c we rotate x -> y and y->x
# this averages the two
pvalues.cors<-0.5*(pvalues.cors + t(pvalues.cors))

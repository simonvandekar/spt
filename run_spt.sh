# number to start at (usually 1)
start=$1
# number to end at.
NBOOT=$2
# number of neighbors to average for interpolation. Not needed for nearest neighbot interp
nneigh=5
# power for inverse distance weighting. Not needed for nearest neighbor interp
p=2
# interpolation style. "idw" for inverse distance weighted or "nn" for nearest neighbor
interp="nn"
# queues on the grid to use
queue=noxnat.q,long.q
# place to store the log and error files
logdir=/tmp/spt_logdir
outdir=$(pwd)/spt_out
file=$(pwd)/data_frame_format.rds
mkdir $logdir $outdir 2>/dev/null
rm -f $logdir/*
echo qsub -S /bin/bash -t $start-$NBOOT -cwd -o $logdir -e $logdir -q $queue /import/monstrum2/Applications/R3.2.3/bin/R --file=$(pwd)/spt.R --slave --args $file $outdir $interp $nneigh $p

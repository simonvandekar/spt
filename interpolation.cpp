#include <RcppArmadillo.h>
//#include <armadillo>
#include <cmath>
#include <Rmath.h>
#include <algorithm>
#include <random>

// see http://stackoverflow.com/questions/29311481/sourcecpp-upcoming-iso-standard
// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::depends(RcppArmadillo)]]

using namespace arma;
using namespace Rcpp;

// [[Rcpp::export]]
mat interpolate( mat newcoord, mat oldcoord, mat measures, int nneigh, float p){
  //vec test = sum(square(oldcoord), 1);
  vec norm1 = max(sum(square(oldcoord), 1));
  vec norm2 = max(sum(square(newcoord), 1));
  float norm = (norm1(0)<norm2(0))?norm2(0):norm1(0);
  mat outmeasures(measures.n_rows, measures.n_cols);
  for(int i=0; i < oldcoord.n_rows; i++){
    //vec mins = (newcoord * oldcoord.row(i).t() )/norm(0);
    //Rcout << mins << '\n';
    vec mins = arma::acos((newcoord * oldcoord.row(i).t() )/norm);
    uvec ords = sort_index(mins);
    ords.resize(nneigh);
    vec invdist = 1/square(mins(ords));
    outmeasures.row(i) = (invdist.t() * measures.rows(ords))/sum(invdist);
  }
  return(outmeasures);
}

// [[Rcpp::export]]
mat nninterpolate( mat newcoord, mat oldcoord, mat measures){
  vec norm1 = max(sum(square(oldcoord), 1));
  vec norm2 = max(sum(square(newcoord), 1));
  float norm = (norm1(0)<norm2(0))?norm2(0):norm1(0);
  mat outmeasures(measures.n_rows, measures.n_cols);
  for(int i=0; i < oldcoord.n_rows; i++){
    vec mins = arma::acos((newcoord * oldcoord.row(i).t() )/norm);
    int j = index_min(mins);
    outmeasures.row(i) = measures.row(j);
  }
  return(outmeasures);
}
